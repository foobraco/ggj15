﻿using UnityEngine;
using System.Collections;

public class PlatformSwitchable : MonoBehaviour {

	public SwitchScript switch1, switch2;
	private MovingPlatformScript platform;

	// Use this for initialization
	void Awake () {
		platform = GetComponent<MovingPlatformScript>();
		switch1.onActive += movePlatform;
		switch2.onActive += movePlatform;
        switch1.onDeactive += Dummy;
        switch2.onDeactive += Dummy;
	}
	
	void movePlatform()
	{
		platform.Move();
	}

    void Dummy()
    { 
        
    }
}
