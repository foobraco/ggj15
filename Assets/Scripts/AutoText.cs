﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AutoText : MonoBehaviour{

	[Multiline]
	public string textToType;
	static Text activeText;
	public static bool isTyping;
	public float delayTime;
    public bool useEffect;

	public static void TypeText(Text textElement, string text, float time){
		activeText = textElement;
		float characterDelay = time / text.Length;
		textElement.text = "";
		if(isTyping)
		{
			textElement.StopAllCoroutines();
		}
		isTyping = true;
		textElement.StartCoroutine(SetText(textElement, text, characterDelay));
	}
	
	static IEnumerator SetText(Text textElement, string text, float characterDelay){
		for(int i=0; i<text.Length; i++){
			textElement.text += text[i];
			yield return new WaitForSeconds(characterDelay);
		}
		isTyping = false;
	}
	
	public static void stopTyping()
	{
		if(isTyping)
		{
			activeText.StopAllCoroutines();
			isTyping = false;
		}
	}

	public void OnEnable ()
	{
			assignText();
	}

	public void OnDisable ()
	{

	}

	void assignText()
	{
		AutoText.TypeText(GetComponent<Text>(), textToType, delayTime);
	}

    void Update()
    {
        if (!isTyping && useEffect)
        {
            GetComponent<Text>().color = new Color(Random.Range(0, 255), Random.Range(0, 255), Random.Range(0, 255));
        }
    }
}