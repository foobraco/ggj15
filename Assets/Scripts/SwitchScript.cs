﻿using UnityEngine;
using System.Collections;

public class SwitchScript : MonoBehaviour {

	public delegate void onActiveSwtich();

	[HideInInspector]
	public bool isActive;

	public onActiveSwtich onActive;

	public delegate void onDeactiveSwitch();

	public onDeactiveSwitch onDeactive;

	// Use this for initialization
	void Awake () {
		isActive = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

}
