﻿using UnityEngine;
using System.Collections;

public class SwitchableLevel1 : MonoBehaviour {

	public SwitchScript _switch;

	// Use this for initialization
	void Awake () {
		_switch.onActive += Test;
		_switch.onDeactive += deactiveTest;
	}

	void Test()
	{
		GameObject.FindGameObjectWithTag("Door").GetComponent<DoorScript>().addNumber();
        if(renderer)
		    renderer.material.color = Color.green;
		_switch.isActive = true;
	}

	void deactiveTest()
	{
		var door = GameObject.FindGameObjectWithTag("Door").GetComponent<DoorScript>();
		if(door.currentNumber < door.numberToOpen)
		{
			door.takeNumber();
		}
        if (renderer)
            renderer.material.color = Color.white;
		_switch.isActive = false;
	}

	// Update is called once per frame
	void Update () {

	}
}
