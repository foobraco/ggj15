﻿using UnityEngine;
using System.Collections;

public class CameraRotationScript : MonoBehaviour {
    float rot;
    float rotSpeed;
    public Transform trans;
	// Use this for initialization
	void Start () {
        rotSpeed = 0.5f;
        rot = 90;
	}
	
	// Update is called once per frame
	void Update () {
        transform.RotateAround(trans.position, Vector3.up, Mathf.Lerp(0, rot, Time.deltaTime * rotSpeed));
        
	}
}
