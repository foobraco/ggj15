﻿using UnityEngine;
using InControl;
using System.Collections;

public class MovementScript : MonoBehaviour {

	CharacterController controller;
    Camera attachedCamera;
    public float gravityValue = 9.8f;
    public int deviceIndex;
    public float jumpImpulse = 4f;
    float velocityY;
    bool canJump = false;
    Animator animator;
	// Use this for initialization
	void Awake () {
		controller = GetComponent<CharacterController>();
        attachedCamera = transform.GetComponentInChildren<Camera>();
        animator = transform.GetComponentInChildren<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        if (controller.isGrounded)
        {
            if (!canJump)
            { 
                    transform.parent = null; 
            }

            canJump = true;

            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                if (hit.collider.name.Equals("MovingPlatform"))
                {
                    transform.parent = hit.collider.transform;
                }
                
            }
        }

        
            if (deviceIndex < InputManager.Devices.Count)
            {
                var device = InputManager.Devices[deviceIndex];

                Vector3 forward = attachedCamera.transform.forward;
                Vector3 right = attachedCamera.transform.right;

                controller.Move(forward * device.Direction.Y);
                controller.Move(right * device.Direction.X);

                if (controller.velocity.x != 0)
                {
                    animator.Play(Animator.StringToHash("Walk"));
                }
                else {
                    animator.Play(Animator.StringToHash("Idle"));
                }

                if (device.Action1.WasPressed && canJump)
                {
                    velocityY = jumpImpulse;
                    canJump = false;
                }

				
            }

            velocityY -= gravityValue * Time.deltaTime;
            controller.Move(Vector3.up * (velocityY));
	}

	void OnTriggerEnter(Collider col)
	{
		Debug.LogWarning(col.name);
		if(col.CompareTag("Exit"))
		{
			col.GetComponent<MovetoScene>().goToScene();
		}
	}

}
