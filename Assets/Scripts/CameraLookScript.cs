﻿using UnityEngine;
using InControl;
using System.Collections;

public class CameraLookScript : MonoBehaviour {

	// Use this for initialization
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    float rotationY = 0F;
    public int deviceIndex;

    void Update()
    {
        if (InputManager.Devices.Count - 1 < deviceIndex)
            return;

        var device = InputManager.Devices[deviceIndex];

        if (axes == RotationAxes.MouseXAndY)
        {
            float rotationX = transform.localEulerAngles.y + device.RightStick.X * sensitivityX;

            rotationY += device.RightStick.Y * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
        else if (axes == RotationAxes.MouseX)
        {
            transform.Rotate(0, device.RightStick.X * sensitivityX, 0);
        }
        else
        {
            rotationY += device.RightStick.Y * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
        }
    }
}
