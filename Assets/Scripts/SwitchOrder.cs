﻿using UnityEngine;
using System.Collections;

public class SwitchOrder : MonoBehaviour {

	public SwitchScript switch1, switch2, switch3;

	[HideInInspector]
	public bool sw1, sw2, sw3;
	[HideInInspector]
	public int tries;
	bool hasOpenDoor;

	// Use this for initialization
	void Awake () {
		tries = 0;
		hasOpenDoor = false;
		switch1.onActive += enablesw1;
		switch1.onDeactive += disablesw1;
		switch2.onActive += enablesw2;
		switch2.onDeactive += disablesw2;
		switch3.onActive += enablesw3;
		switch3.onDeactive += disablesw3;
	}
	
	// Update is called once per frame
	void Update () {
		if(sw1 && sw2 && sw3 && !hasOpenDoor)
		{
			hasOpenDoor = true;
			Debug.Log("add number to door");
			GameObject.FindGameObjectWithTag("Door").GetComponent<DoorScript>().addNumber();
			switch1.onActive -= enablesw1;
			switch1.onDeactive -= disablesw1;
			switch2.onActive -= enablesw2;
			switch2.onDeactive -= disablesw2;
			switch3.onActive -= enablesw3;
			switch3.onDeactive -= disablesw3;

		}else
		{
			if(tries >= 3)
			{
				tries = 0;
				sw1 = sw2 = sw3 = false;
				if(!hasOpenDoor)
				{
					switch1.onDeactive();
					switch2.onDeactive();
					switch3.onDeactive();			}
				}
		}
	}

	void enablesw1()
	{
		Debug.Log("switch active");
		tries++;
		switch1.transform.GetChild(0).GetChild(0).renderer.material.color = Color.blue;
		if(sw2 && sw3)
		{
			sw1 = true;
		}
	}

	void disablesw1()
	{
		switch1.transform.GetChild(0).GetChild(0).renderer.material.color = Color.white;
		tries--;
	}

	void enablesw2()
	{
		Debug.Log("switch active");
		switch2.transform.GetChild(0).GetChild(0).renderer.material.color = Color.blue;
		tries++;
		sw2 = true;
	}
	
	void disablesw2()
	{
		switch2.transform.GetChild(0).GetChild(0).renderer.material.color = Color.white;
		tries--;
	}

	void enablesw3()
	{
		Debug.Log("switch active");
		switch3.transform.GetChild(0).GetChild(0).renderer.material.color = Color.blue;
		tries++;
		if(sw2)
		{
			sw3 = true;
		}
	}
	
	void disablesw3()
	{
		switch3.transform.GetChild(0).GetChild(0).renderer.material.color = Color.white;
		tries--;
	}

}
