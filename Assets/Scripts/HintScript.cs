﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HintScript : MonoBehaviour {


	public float timeToFade;
	Text text;
	bool startfading;
	Color transparentColor;

	// Use this for initialization
	void Awake () {
		startfading = false;
		Invoke("startfade",timeToFade);
		text = GetComponent<Text>();
		transparentColor = text.color;
		transparentColor.a = 0f;
	}

	void Update ()
	{
		if(startfading)
			text.color = Color.Lerp(text.color, transparentColor, Time.deltaTime);
	}

	void startfade ()
	{
		startfading = true;	
	}

}
