﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {
    public Transform childLeft;
    public Transform childRight;

	public int numberToOpen;

	[HideInInspector]
    public int currentNumber;
	private bool isOpen;

    Vector3 leftInitialPos, rightInitialPos;
    Vector3 leftDesiredPos, rightDesiredPos;
	// Use this for initialization
	void Start () {
		currentNumber = 0;
		isOpen = false;
        leftInitialPos = childLeft.transform.position;
        rightInitialPos = childRight.transform.position;
        leftDesiredPos = leftInitialPos;
        rightDesiredPos = rightInitialPos;
//        Invoke("Open", 5f);
//        Invoke("Close", 15f);
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log("door number "+currentNumber);
		if(numberToOpen == currentNumber && !isOpen)
		{
			Open();
		}

		if(currentNumber < numberToOpen && isOpen)
		{
			Close();
		}
        childLeft.transform.position = Vector3.Lerp(childLeft.transform.position, leftDesiredPos, Time.deltaTime);
        childRight.transform.position = Vector3.Lerp(childRight.transform.position, rightDesiredPos, Time.deltaTime);
	}


	public void addNumber()
	{
		currentNumber++;
	}

	public void takeNumber()
	{
		currentNumber--;
	}

    public void Open()
    {
		isOpen = true;
        rightDesiredPos = new Vector3(childRight.transform.position.x - 10f, childRight.transform.position.y, childRight.transform.position.z);
        leftDesiredPos = new Vector3(childLeft.transform.position.x + 10f, childLeft.transform.position.y, childLeft.transform.position.z);

    }

    public void Close()
    {
		isOpen = false;
        rightDesiredPos = rightInitialPos;
        leftDesiredPos = leftInitialPos;
    }

}
