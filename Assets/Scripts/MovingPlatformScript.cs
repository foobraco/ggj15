﻿using UnityEngine;
using System.Collections;

public class MovingPlatformScript : MonoBehaviour {
    public Vector3 InitialPos;
    public Vector3 DesiredPos;
    Vector3 destination;
    public float precision = 3f;
	// Use this for initialization
	void Start () {
        destination = DesiredPos;
	}
	
	// Update is called once per frame
	void Update () {
//        Move();
	}

    public void Move()
    {
        transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime);
        if (AproxVectors(transform.position, destination))
        {
            if (AproxVectors(transform.position, DesiredPos))
                destination = InitialPos;
            if (AproxVectors(transform.position, InitialPos))
                destination = DesiredPos;

        }
    }

    bool AproxVectors(Vector3 one, Vector3 two)
    {
        if ((Mathf.Abs(one.x - two.x) < precision) && (Mathf.Abs(one.y - two.y) < precision) && (Mathf.Abs(one.z - two.z) < precision))
            return true;

        return false;
    }
}
