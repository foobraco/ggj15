﻿using UnityEngine;
using InControl;
using System.Collections;

public class ObjectDetection : MonoBehaviour {

	int index;
	Collider activeHit;
	bool isActive;

	void Awake () {
		index = GetComponent<CameraLookScript>().deviceIndex;
	}
	
	// Update is called once per frame
	void Update () {
		if (InputManager.Devices.Count - 1 < index)
			return;

		if(InputManager.Devices[index].Action3.IsPressed)
		{
			Debug.DrawLine(transform.position, transform.GetChild(0).position, Color.cyan,0.5f);
			RaycastHit hit = new RaycastHit();	
			if(Physics.Linecast(transform.position, transform.GetChild(0).position,out hit))
			{

				if(hit.collider.CompareTag("TempSwitch") && !isActive)
				{
					Debug.Log("collider name "+hit.collider.name);
					hit.collider.gameObject.GetComponent<SwitchScript>().onActive();
					hit.collider.gameObject.GetComponent<SwitchScript>().isActive = true;
					isActive = true;
					activeHit = hit.collider;
                    //transform.parent.GetComponent<AudioSource>().Play();
                }
                else if (hit.collider.CompareTag("TempSwitch") && isActive)
                {
                    Debug.Log("collider name " + hit.collider.name);
                    hit.collider.gameObject.GetComponent<SwitchScript>().onDeactive();
                    hit.collider.gameObject.GetComponent<SwitchScript>().isActive = false;
                    isActive = false;
                    activeHit = hit.collider;
                   // transform.parent.GetComponent<AudioSource>().Play();
                }
				
			}else
			{
				
			}
		}else
		{
			
		}

		if(InputManager.Devices[index].Action3.WasPressed)
		{
			RaycastHit hit = new RaycastHit();	
			if(Physics.Linecast(transform.position, transform.GetChild(0).position,out hit))
			{
				if(hit.collider.CompareTag("Switch"))
				{
					var sw = hit.collider.gameObject.GetComponent<SwitchScript>();
					if(sw.isActive)
					{
						sw.onDeactive();
						isActive = false;
						sw.isActive = false;
						activeHit = hit.collider;
					}
					else
					{
						sw.onActive();
						isActive = true;
						sw.isActive = true;
						activeHit = null;
					}
				}
			}
		}
		
	}
}
