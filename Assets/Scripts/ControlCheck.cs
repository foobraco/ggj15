﻿using UnityEngine;
using InControl;
using UnityEngine.UI;
using System.Collections;

public class ControlCheck : MonoBehaviour {

	public GameObject panel;

	// Use this for initialization
	void Awake () {

	}
	
	// Update is called once per frame
	void Update () {
		if(InputManager.Devices.Count < 2)
		{
			panel.SetActive(true);
		}
		else
		{
			panel.SetActive(false);
		}
	}
}
